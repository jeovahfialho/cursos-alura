/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    Image, 
    Dimensions, 
    FlatList
  } from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

const width = Dimensions.get('screen').width;
const height = Dimensions.get('screen').height;

type Props = {};

export default class AppImage extends Component<Props> {
  render() {

      const fotos = [
        { id: 1, usuario: 'rafael'},
        { id: 2, usuario: 'alberto'},
        { id: 3, usuario: 'vitor'}
      ];

      return (
        <FlatList style={{marginTop: 20}}>
        data={fotos}
        keyExtractor={item => item.id}
        renderItem = {({item}) =>
          <View key={foto.id}>
            <Text>{foto.usuario}</Text>
            <Image 
              source={require('./resources/img/background.jpg')} 
              style={{ width: width, height: width}}
            />
          </View>
        }
        </FlatList>
      );
  }
}